# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 07:08+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Control/TextEdit/.project:24
msgid "TextEditExample"
msgstr "Пример текстового редактора"

#: app/examples/Control/TextEdit/.project:25
msgid "A simple TextEdit example"
msgstr "Простой пример текстового редактора"

#: app/examples/Control/TextEdit/.src/FMain.class:151
msgid ""
"A simple TextEdit example by\n"
"Fabien Bodard (gambix@users.sourceforge.net)\n"
"and Benoît Minisini"
msgstr ""
"Простой пример текстового редактора\n"
"Фабьена Бодарда (gambix@users.sourceforge.net)\n"
"и Бенуа Минисини"

#: app/examples/Control/TextEdit/.src/FMain.form:5
msgid "Text Editor"
msgstr "Текстовый редактор"

#: app/examples/Control/TextEdit/.src/FMain.form:14
msgid "Read-only"
msgstr "Только чтение"

#: app/examples/Control/TextEdit/.src/FMain.form:23
msgid "Bold"
msgstr "Полужирный"

#: app/examples/Control/TextEdit/.src/FMain.form:29
msgid "Italic"
msgstr "Курсив"

#: app/examples/Control/TextEdit/.src/FMain.form:35
msgid "Underline"
msgstr "Подчёркнутый"

#: app/examples/Control/TextEdit/.src/FMain.form:41
msgid "StrikeOut"
msgstr "Зачёркнутый"

#: app/examples/Control/TextEdit/.src/FMain.form:53
msgid "Align Left"
msgstr "Выровнять влево"

#: app/examples/Control/TextEdit/.src/FMain.form:60
msgid "Align Center"
msgstr "Выровнять по центру"

#: app/examples/Control/TextEdit/.src/FMain.form:67
msgid "Align Right"
msgstr "Выровнять вправо"

#: app/examples/Control/TextEdit/.src/FMain.form:74
msgid "Align Justify"
msgstr "Выровнять по привязке"

#: app/examples/Control/TextEdit/.src/FMain.form:85
msgid "Set font face"
msgstr "Установить начертание шрифта"

#: app/examples/Control/TextEdit/.src/FMain.form:87
msgid "ComboBox1"
msgstr "Комбинированный_список_1"

#: app/examples/Control/TextEdit/.src/FMain.form:94
msgid "Set font size"
msgstr "Установить размер шрифта"

#: app/examples/Control/TextEdit/.src/FMain.form:106
msgid "Set font color"
msgstr "Установить цвет шрифта"

#: app/examples/Control/TextEdit/.src/FMain.form:115
msgid "Set background color"
msgstr "Установить цвет фона"

#: app/examples/Control/TextEdit/.src/FMain.form:122
msgid "Show html source code"
msgstr "Показать исходный код HTML"

#: app/examples/Control/TextEdit/.src/FMain.form:127
msgid "About this example..."
msgstr "Об этом примере..."

#: app/examples/Control/TextEdit/.src/frmShowHtml.form:5
msgid "HtmlSource"
msgstr "Исходник HTML"

#: app/examples/Control/TextEdit/.src/frmShowHtml.form:24
msgid "Close"
msgstr "Закрыть"

