# Catalan translation of Puzzle1To8
# Copyright (C) 2000-2010 Benoît Minisini.
# This file is distributed under the same license as the Puzzle1To8 package.
# Jordi Sayol <g.sayol@yahoo.es>, 2007-2010.
#
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Puzzle1To8 3.13.90\n"
"PO-Revision-Date: 2019-06-04 19:27 UTC\n"
"Last-Translator: benoit <benoit@benoit-kubuntu>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Puzzle 1 to 8"
msgstr "Puzzle 1 a 8"

#: .project:2 FrmAyuda.form:19
msgid "Place the numbers 1 to 8 in each of the boxes without repeating, so that each box does not contain a correlative number to its neighbors or adjacent boxes. See the following examples:"
msgstr "Poseu els números de l'1 al 8 a cada una de les caixes sense repetir-los, en que cada caixa no contingui números correlatius amb les caixes veïnes o adjacents. Mireu l'exemple següent:"

#: FMain.class:69
msgid "Are you sure?"
msgstr "N'esteu segurs?"

# gb-ignore
#: FMain.class:69
msgid "No"
msgstr ""

#: FMain.class:69
msgid "Yes"
msgstr "Sí"

#: FMain.class:90
msgid "Congratulations! You did it!"
msgstr "Felicitats! Ho heu fet!"

#: FMain.form:22
msgid "Puzzle 1 to 8 - Locate the numbers!"
msgstr "Puzle 1 a 8 - Troba els números!"

#: FMain.form:27
msgid "Game"
msgstr "Joc"

#: FMain.form:30
msgid "Clear"
msgstr "Neteja"

#: FMain.form:35
msgid "Quit"
msgstr "Surt"

#: FMain.form:41
msgid "Help"
msgstr "Ajuda"

#: FMain.form:44 FrmAyuda.form:14
msgid "How to play?"
msgstr "Com jugar-hi?"

#: FMain.form:49 FrmAbout.form:12
msgid "About"
msgstr ""

#: FrmAbout.form:21
msgid "Author"
msgstr "Autor"

#: FrmAbout.form:25
msgid "Author: Pablo Mileti.<br> <br>You can submit your questions, suggestions, bugs, etc, to the following E-Mail address:\n<a href=\"mailto:pablomileti@gmail.com\"> pablomileti@gmail.com</a>"
msgstr "Autor: Pablo Mileti.<br> <br>Podeu enviar les vostres preguntes, suggeriments, errors, etc. a  aquesta adreça de correu electrònic:\n<a href=\"mailto:pablomileti@gmail.com\"> pablomileti@gmail.com</a>"

#: FrmAbout.form:29
msgid "License"
msgstr "Llicència"

#: FrmAbout.form:33
msgid "\n    Copyright (C) 2010. Author: Pablo Mileti  \n\nThis program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>."
msgstr "\n    Copyright (C) 2010. Autor: Pablo Mileti  \n\nAquest programa és programari lliure; el podeu distribuir i/o modificar sota els termes de la llicència GNU General Public License  tal i com està publicada per la Free Software Foundation; ja sigui la versió 3 de la llicència, o bé (si ho preferiu) qualsevol versió posterior.\n\nAquest programa es distribueix amb la voluntat que pugui ser útil però SENSE CAP GARANTIA; ni tant sols les garanties implícites MERCANTILS o ESPECÍFIQUES PER UN PROPÒSIT DETERMINAT. Si voleu més informació, vegeu la llicència GNU General Public Licence.\n\nHauríeu d'haver rebut una còpia de la GNU General Public Licence juntament amb aquest programa. Si no fos així, mireu <http://www.gnu.org/licenses/>."

#: FrmAbout.form:43 FrmAyuda.form:43
msgid "Close"
msgstr ""

#: FrmAyuda.form:31
msgid "<b>Way incorrect:</b><br><br>The numbers 1 and 2 are consecutive and are in adjoining boxes, so it is not allowed."
msgstr "<b>Manera incorrecta:</b><br><br>Els números 1 i 2 són consecutius i estan en caixes adjacents, o sigui que no està permès."

#: FrmAyuda.form:48
msgid "<b>Way correct:</b><br><br>There aren't correlativity between to the numbers entered with respect to their adjoining boxes, so it's allowed."
msgstr "<b>Manera correcta:</b><br><br>No hi ha correlativitat entre els números respecte de les caixes adjacents, o sigui que està permès."

