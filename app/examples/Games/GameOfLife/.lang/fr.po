#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: GameOfLife 3.18.90\n"
"PO-Revision-Date: 2023-09-15 17:08 UTC\n"
"Last-Translator: benoit <benoit@benoit-TOWER>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: FMain.form:167
msgid "0 Neighbour"
msgstr "0 voisins"

#: FMain.form:160
msgid "1 Neighbour"
msgstr "1 voisin"

#: FMain.form:153
msgid "2 Neighbours"
msgstr "2 voisins"

#: FMain.form:147
msgid "3 Neighbours"
msgstr "3 voisins"

#: FMain.form:140
msgid "4 Neighbours"
msgstr "4 voisins"

#: FMain.form:133
msgid "5 Neighbours"
msgstr "5 voisins"

#: FMain.form:126
msgid "6 Neighbours"
msgstr "6 voisins"

#: FMain.form:119
msgid "7 Neighbours"
msgstr "7 voisins"

#: FMain.form:112
msgid "8 Neighbours"
msgstr "8 voisins"

#: FMain.form:193
msgid "Alive"
msgstr "Vivant"

#: FMain.form:199
msgid "Dead"
msgstr "Mort"

#: FMain.form:204
msgid "Draw cell borders"
msgstr "Dessiner les bords des cellules"

#: FMain.class:72
msgid "Evolution Delay: "
msgstr "Période d'évolution : "

#: FMain.form:101
msgid "Evolution Delay: 20ms"
msgstr "Période d'évolution : 20 ms"

#: .project:1
msgid "Game of Life"
msgstr "Le jeu de la vie"

#: FMain.form:48
msgid "GameOfLife"
msgstr ""

#: FMain.form:219
msgid "Horizontal symmetry"
msgstr "Symétrie horizontale"

#: FMain.class:72
msgid "ms"
msgstr "ms"

#: FMain.form:178
msgid "Options"
msgstr "Options"

#: FMain.form:174
msgid "Select the Count of Neighbours where a cell will die or keep its state."
msgstr "Sélectionnez le nombre de voisins pour lequel la cellule mourra ou vivra."

#: FMain.form:182
msgid "Set here the probability that a Cell will be born or not when you spawn the first Generation."
msgstr "Sélectionnez la probabilité qu'une cellule naisse à la première génération."

#: FMain.form:209
msgid "Small generation"
msgstr "Petite génération"

#: FMain.form:78
msgid "Spawn First Generation"
msgstr "Créer la première génération"

#: FMain.form:96
msgid "Start Evolution"
msgstr "Démarrer l'évolution"

#: FMain.form:108
msgid "Survival"
msgstr "Survie"

#: FMain.form:37
msgid "The Game of Life"
msgstr "Le jeu de la vie"

#: .project:2
msgid "The Game Of Life.\n\nThis example runs the Game Of Life cellular automaton. It allows to define many parameters of the automaton."
msgstr ""

#: FMain.form:214
msgid "Vertical symmetry"
msgstr "Symétrie verticale"

#: FMain.form:65
msgid "Written in Gambas<br>\nby <b>Iman Karim</b><br>\nand <b>Benoît Minisini</b>\n<p>\n<i>Thanks to the Gambas team!</i>"
msgstr "Réalisé en Gambas<br>\npar <b>Iman Karim</b><br>\n et <b>Benoît Minisini</b>\n<p>\n<i>Merci à toute l'équipe de Gambas !</i>"

