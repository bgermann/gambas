/***************************************************************************

  gbc_project.h

  (c) Benoît Minisini <benoit.minisini@gambas-basic.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

***************************************************************************/

#ifndef __GBC_PROJECT_H
#define __GBC_PROJECT_H

typedef
	bool (*PROJECT_BROWSE_FUNC)(const char *key, int len_key, const char *value, int len_value);

typedef
	void (*PROJECT_BROWSE_VALUE)(const char *value, int len);

#define PROJECT_is_key(_key, _len, _comp) (_len == strlen(_comp) && strncmp(_key, _comp, _len) == 0)
	
bool PROJECT_load(const char *path);
void PROJECT_browse(PROJECT_BROWSE_FUNC func);
void PROJECT_browse_string_list(const char *value, int len, PROJECT_BROWSE_VALUE func);
void PROJECT_exit(void);

#endif
