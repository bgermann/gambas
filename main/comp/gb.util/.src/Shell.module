' Gambas module file

Export

Public Sub MkDir(Path As String)

  Dim sElt As String
  Dim sMake As String = "/"

  If Path Begins "~/" Then Path = User.Home &/ Mid$(Path, 3)

  For Each sElt In Split(Path, "/")
    sMake &/= sElt
    If IsDir(sMake) Then Continue
    Try Mkdir sMake
  Next
  
  If Not Exist(Path) Or If Not IsDir(Path) Then Error.Raise("Cannot create directory")

End

Public Sub RmDir(Path As String, Optional Force As Boolean)

  Dim sFile As String
  Dim aDir As String[]
  
  If Len(Path) > 1 And If Right(Path) = "/" Then Path = Left(Path, -1)
  If Not Exist(Path) Then Return
  
  If Not Force Then
    If Path = "~" Or If Path = User.Home Or If Path = "/" Then Error.Raise("Removing this directory recursively is a bad idea: " & Path)
  Endif
  
  Try aDir = Dir(Path)
  If aDir Then
    For Each sFile In aDir
      If IsDir(Path &/ sFile) Then
        RmDir(Path &/ sFile)
      Else
        Try Kill Path &/ sFile
      Endif
    Next
  Endif
  
  Rmdir Path
  
End

Public Sub Move(Source As String, Destination As String, Optional Force As Boolean)
  
  If IsDir(Destination) Then Destination &/= File.Name(Source)
  
  If Force Then
    Try Move Source Kill Destination
  Else
    Try Move Source To Destination
  Endif
  If Error Then
    If Force Then Try Kill Destination
    Copy Source To Destination
    Kill Source
  Endif
  
End

'' @{since 3.20}
''
'' Copy a file or a directory recursively.
''
'' - ~Source~ : The source file or directory.
'' - ~Destination~ : The target of the copy.
'' - ~Force~ : If 'True', existing targets are replaced. Otherwise an error is raised.
''
'' Here is the possible behaviours:
''
'' [[
'' If the source is
'' --
'' If the destination is
'' --
'' Behaviour
'' ==
'' An existing directory
'' --
'' An existing directory
'' --
'' Copy the source directory *inside* the destination directory.
'' ==
'' An existing directory
'' --
'' A non-existing path
'' --
'' Copy the source directory to the destination directory.
'' ==
'' A file
'' --
'' An existing directory
'' --
'' Copy the source file into the destination directory.
'' ==
'' A file
'' --
'' A non-existing path.
'' --
'' Copy the source file to the destination path.
'' ]]
''
'' Other possibilities raise an error.

Public Sub Copy(Source As String, Destination As String, Optional Force As Boolean)
  
  Dim sFile As String
  
  If File.IsRelative(Source) Then
    Source = ".." &/ Source
  Endif
  
  If IsDir(Source) Then
    
    If Not Exist(Destination) Then
      Copy(Source, File.Dir(Destination), Force)
      Move File.Dir(Destination) &/ File.Name(Source) To Destination
      Return
    Endif
    
    If Not IsDir(Destination) Then Error.Raise("Destination is not a directory")
    
    Try Mkdir Destination &/ File.Name(Source)
    For Each sFile In Dir(Source)
      Copy(Source &/ sFile, Destination &/ File.Name(Source), Force)
    Next
    
  Else
    
    If IsDir(Destination) Then
      
      If Force Then Try Kill Destination &/ File.Name(Source)
      Copy Source To Destination &/ File.Name(Source)
    
    Else
    
      If Force Then Try Kill Destination
      Copy Source To Destination
    
    Endif
    
  Endif
  
End

