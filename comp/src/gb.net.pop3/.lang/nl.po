# Willy Raets < gbWilly@protonmail.com >, 2014
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.net.pop3 3.10.90\n"
"POT-Creation-Date: 2025-01-04 13:57 UTC\n"
"PO-Revision-Date: 2017-08-26 19:44 UTC\n"
"Last-Translator: Willy Raets <gbWilly@protonmail.com>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "gb.net.pop3"
msgstr "gb.net.pop3"

#: .project:2
msgid "Gambas implementation of the POP3 protocol."
msgstr "Gambas implementatie van het POP3 protocol"
