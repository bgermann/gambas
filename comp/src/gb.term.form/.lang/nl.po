# Willy Raets < gbWilly@protonmail.com >, 2019
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.term.form 3.13.0\n"
"POT-Creation-Date: 2025-01-04 13:58 UTC\n"
"PO-Revision-Date: 2019-05-18 13:57 UTC\n"
"Last-Translator: gbWilly <gbWilly@oprotonmail.com>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:2
msgid ""
"This components allows to dialog with a VT102 terminal.\n"
"\n"
"It provides a way to use common visual objects like windows, standard controls like labels, checkboxes, lists and so on... as in graphical mode.\n"
"\n"
"It will also provide a standalone class with common VT100 commands to be used by more simple applications."
msgstr ""
"Dit component staat de dialoog toe met een VT102 terminal.\n"
"\n"
"Het voorziet in een manier om gebruikelijke visuele objecten zoals vensters, standaart controls zoals labels,checkboxen, en zo verder te gebruiken...zoals in grafische modus.\n"
"\n"
"Het voorziet ook in een alleenstaande klasse met gebruikelijke VT100 commando's voor gebruik in eenvoudigere applicaties."

#: Termform1.termform:17
msgid "Bonjour"
msgstr ""
